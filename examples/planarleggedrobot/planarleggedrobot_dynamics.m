% proNEu: A tool for rigid multi-body mechanics in robotics.
% 
% Copyright (C) 2017  Marco Hutter, Christian Gehring, C. Dario Bellicoso,
% Vassilios Tsounis
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

%% TODO
%   File:           planarleggedrobot_dynamics.m
%
%   Authors:        Manuel Breitenstein, bmanuel@ethz.ch
%
%   Date:           17/10/2017
%
%   Desciption:     Analytical derivation of the EoM for a planar 5 link legged robot 
%                   in a low gravity environment.
%                   File is based on the monopod example of Vassilios
%                   Tsounis, tsounisv@ethz.ch.
%

% Optional calls for workspace resetting/cleaning
close all; clear all; clear classes; clc;

%% World Gravity

% Gravitational potential field in inertial world-frame
syms grav real;
e_I_g = [0 0 1].';
I_a_g = grav*e_I_g;

%% System Parameters & Variables
% Name convention:
%B  = Main body
%Ul = Upper leg left
%Ll = Lower leg left
%FL = Foot left
%Ur = Upper leg right
%Lr = Lower leg right
%Fr = Foot right
%
%           
%       _____B_____
%      /           \
%     /Ul           \Ur
%    /               \
%   |                 |
%   |Ll               |Lr
%   |                 |
%   O Fl              O Fr


% Declare all system parametes
syms m_B m_Ul m_Ur m_Ll m_Lr real;

syms d_Bx d_By d_Bz real; %COM offset
syms l_B w_B h_B

syms d_Ulx d_Uly d_Ulz real;
syms r_Ul l_Ul real;

syms d_Urx d_Ury d_Urz real;
syms r_Ur l_Ur real;

syms d_Llx d_Lly d_Llz real;
syms r_Ll l_Ll real;

syms d_Lrx d_Lry d_Lrz real;
syms r_Lr l_Lr real;


syms r_Fl real;

syms r_Fr real;


% Declare all system variables
syms z_B x_B real;                                    %Initial position of the mainbody
syms phi_B                                            %Initial rotation of the mainbody
syms phi_Ul phi_Ll phi_Ur phi_Lr real;                %Rotation of the legs
syms T_Hl T_Kl T_Hr T_Kr real;                        %Torques at joints

%externam forces and torques at the feet
syms F_Flx F_Fly F_Flz T_Flx T_Fly T_Flz real;        
syms F_Frx F_Fry F_Frz T_Frx T_Fry T_Frz real;        

%% Multi-Body System Description

i=1;
body(i) = RigidBodyDescription_v2;
body(i).name = 'B';
body(i).ktree.nodeid = i;
body(i).ktree.parents = i-1;
body(i).cs.P_r_PB = [x_B 0 z_B].';
body(i).cs.C_PB = getRotationMatrixY(phi_B);
body(i).param.m = m_B;
body(i).param.B_Theta = [];
body(i).param.B_r_BCoM = [d_Bx d_By d_Bz].'; 
body(i).param.C_BCoM = sym(eye(3));
body(i).geometry.type = 'cuboid';
body(i).geometry.issolid = false;
body(i).geometry.params = [l_B w_B h_B] ; 
body(i).geometry.values = [0.1 0.1 2];
body(i).geometry.offsets.r = [0 0 -1].';
body(i).geometry.offsets.C = eye(3);
body(i).geometry.color = [197 172 202]/255;




i=2;
body(i) = RigidBodyDescription_v2;
body(i).name = 'Ul';
body(i).ktree.nodeid = i;
body(i).ktree.parents = i-1;
body(i).cs.P_r_PB = [0 0 0].';
body(i).cs.C_PB = getRotationMatrixY(phi_Ul);
body(i).param.m = m_Ul;
body(i).param.B_Theta = [];
body(i).param.B_r_BCoM = [d_Ulx d_Uly d_Ulz]; 
body(i).param.C_BCoM = sym(eye(3));
body(i).geometry.type = 'cylinder';
body(i).geometry.issolid = false;
body(i).geometry.params = [r_Ul l_Ul]; 
body(i).geometry.values = [0.05 1.0];
body(i).geometry.offsets.r = [0 0 0.5].';
body(i).geometry.offsets.C = eye(3);
body(i).geometry.color = [0.6 0.6 0.6];

i=3;
body(i) = RigidBodyDescription_v2;
body(i).name = 'Ll';
body(i).ktree.nodeid = i;
body(i).ktree.parents = i-1;
body(i).cs.P_r_PB = [0 0 -l_Ul].';
body(i).cs.C_PB = getRotationMatrixY(phi_Ll);
body(i).param.m = m_Ll;
body(i).param.B_Theta = [];
body(i).param.B_r_BCoM = [d_Llx d_Lly d_Llz]; 
body(i).param.C_BCoM = sym(eye(3));
body(i).geometry.type = 'cylinder';
body(i).geometry.issolid = false;
body(i).geometry.params = [r_Ll l_Ll]; 
body(i).geometry.values = [0.05 1.0];
body(i).geometry.offsets.r = [0 0 0.5].';
body(i).geometry.offsets.C = eye(3);
body(i).geometry.color = [0.6 0.6 0.6];

i=4;
body(i) = RigidBodyDescription_v2;
body(i).name = 'Fl';
body(i).ktree.nodeid = i;
body(i).ktree.parents = i-1;
body(i).cs.P_r_PB = [0 0 -l_Ll].';
body(i).cs.C_PB = sym(eye(3));
body(i).geometry.type = 'sphere';
body(i).geometry.issolid = false;
body(i).geometry.params = [r_Fl]; 
body(i).geometry.values = [0.07];
body(i).geometry.offsets.r = [0 0 0].';
body(i).geometry.offsets.C = eye(3);
body(i).geometry.color = [0.8 0.6 0.6];

%Right side:
i=5;
body(i) = RigidBodyDescription_v2;
body(i).name = 'Ur';
body(i).ktree.nodeid = 5;
body(i).ktree.parents = 1;
body(i).cs.P_r_PB = [0 0 h_B].';
body(i).cs.C_PB = getRotationMatrixY(phi_Ur);
body(i).param.m = m_Ur;
body(i).param.B_Theta = [];
body(i).param.B_r_BCoM = [d_Urx d_Ury d_Urz]; 
body(i).param.C_BCoM = sym(eye(3));
body(i).geometry.type = 'cylinder';
body(i).geometry.issolid = false;
body(i).geometry.params = [r_Ur l_Ur]; 
body(i).geometry.values = [0.05 1.0];
body(i).geometry.offsets.r = [0 0 -0.5].';
body(i).geometry.offsets.C = eye(3);
body(i).geometry.color = [0.6 0.6 0.6];

i=6;
body(i) = RigidBodyDescription_v2;
body(i).name = 'Lr';
body(i).ktree.nodeid = i;
body(i).ktree.parents = i-1;
body(i).cs.P_r_PB = [0 0 l_Ur].';
body(i).cs.C_PB = getRotationMatrixY(phi_Lr);
body(i).param.m = m_Lr;
body(i).param.B_Theta = [];
body(i).param.B_r_BCoM = [d_Lrx d_Lry d_Lrz]; 
body(i).param.C_BCoM = sym(eye(3));
body(i).geometry.type = 'cylinder';
body(i).geometry.issolid = false;
body(i).geometry.params = [r_Lr l_Lr]; 
body(i).geometry.values = [0.05 1.0];
body(i).geometry.offsets.r = [0 0 -0.5].';
body(i).geometry.offsets.C = eye(3);
body(i).geometry.color = [0.6 0.6 0.6];

i=7;
body(i) = RigidBodyDescription_v2;
body(i).name = 'Fr';
body(i).ktree.nodeid = i;
body(i).ktree.parents = i-1;
body(i).cs.P_r_PB = [0 0 l_Lr].';
body(i).cs.C_PB = sym(eye(3));
body(i).geometry.type = 'sphere';
body(i).geometry.issolid = false;
body(i).geometry.params = [r_Fr]; 
body(i).geometry.values = [0.07];
body(i).geometry.offsets.r = [0 0 0].';
body(i).geometry.offsets.C = eye(3);
body(i).geometry.color = [0.8 0.6 0.6];
%% Definition of External Forces & Torques
%Version with Wrench

%Left side
j=1;
ftel(j) = ForceTorqueDescription_v2;
ftel(j).name    = 'T_HFEl';
ftel(j).type    = 'rotational';
ftel(j).body_P  = 1; 
ftel(j).body_B  = 2;
ftel(j).B_T     = [0 T_Hl 0].';

j=2;
ftel(j) = ForceTorqueDescription_v2;
ftel(j).name    = 'T_KFEl';
ftel(j).type    = 'rotational';
ftel(j).body_P  = 2; 
ftel(j).body_B  = 3;
ftel(j).B_T     = [0 T_Kl 0].';

j=3;
ftel(j) = ForceTorqueDescription_v2;
ftel(j).name    = 'W_Fl';
ftel(j).type    = 'wrench';
ftel(j).body_P  = 0; 
ftel(j).body_B  = 4;
ftel(j).P_r_R   = sym([0 0 0].');
ftel(j).B_r_A   = sym([ 0 0 -r_Fl].');   
ftel(j).I_F     = [F_Flx F_Fly F_Flz].';   
ftel(j).I_T     = [T_Flx T_Fly T_Flz].';

%Right side
j=4;
ftel(j) = ForceTorqueDescription_v2;
ftel(j).name    = 'T_HFEr';
ftel(j).type    = 'rotational';
ftel(j).body_P  = 1; 
ftel(j).body_B  = 5;
ftel(j).B_T     = [0 T_Hr 0].';

j=5;
ftel(j) = ForceTorqueDescription_v2;
ftel(j).name    = 'T_KFEr';
ftel(j).type    = 'rotational';
ftel(j).body_P  = 5; 
ftel(j).body_B  = 6;
ftel(j).B_T     = [0 T_Kr 0].';

j=6;
ftel(j) = ForceTorqueDescription_v2;
ftel(j).name    = 'W_Fr';
ftel(j).type    = 'wrench';
ftel(j).body_P  = 0; 
ftel(j).body_B  = 7;
ftel(j).P_r_R   = sym([0 0 0].');
ftel(j).B_r_A   = sym([ 0 0 r_Fr].');   
ftel(j).I_F     = [F_Frx F_Fry F_Frz].';   
ftel(j).I_T     = [T_Frx T_Fry T_Frz].';

%% System Definitions

% Definition of the joint DoFs of 5-link system
q_j  = [z_B x_B phi_B phi_Ul phi_Ll phi_Ur phi_Lr].';

% Controllable joint forces/torques
tau_j = [T_Hl T_Kl T_Hr T_Kr].';

% External forces and torques
tau_env = [F_Flx F_Fly F_Flz T_Flx T_Fly T_Flz F_Frx F_Fry F_Frz T_Frx T_Fry T_Frz].';
%TODO delete this line if not needed. Only in plane torques and forces.: tau_env = [F_Flx F_Flz T_Fly F_Frx F_Frz T_Fry].';

%% Generate Full System Model using proNEu.v2

% Give a name to the model
robot_name = 'PlanarleggedrobotModel';

% Generate the model object
robotmdl = RobotModel(body, ftel, q_j, tau_j, tau_env, I_a_g, 'name', robot_name, 'type', 'fixed', 'method', 'proneu', 'symsteps', 100);

%% Symbolic Simplifications (Advanced)

% Set number of symbolic simplification steps
ns = 100;

% Test symbolic simplifciations of M,b,g - use serial computations
robotmdl.dynamics.symbols.simplifydynamics('elementwise', ns);

%% Generate MATLAB numerical functions

% Generate numerical functions
robotmdl.generatefunctions();
%% Generate Free Floating Taskspace functions

%Get transform to define task space
T_bar_B = robotmdl.body(1).kinematics.symbols.T_IBi;

T_bar_l = robotmdl.body(4).kinematics.symbols.T_IBi;
T_bar_r = robotmdl.body(7).kinematics.symbols.T_IBi;

% Vector in base frame from base frame origin to right hip
r_BHr = [0 0 h_B].';
r_OHr= T_bar_B * [ r_BHr ; 1];
% Z Component of right hip in initial frame
z_Hr = r_OHr(3,1);

% Calculation of the d taskspace variable

% Right an left foot vector in Initial frame
r_OFl = T_bar_l(1:3,4);
r_OFr = T_bar_r(1:3,4);
% Vector from left foot to the center of the distance beetween the right and left foot.
r_FlFm = 0.5*(r_OFr-r_OFl);
% Vector to the center of the distance beetween the right and left foot in
% initial frame
r_OFm = r_OFl + r_FlFm;
x_OFm = r_OFm(1,1);

% Vector to the center of the base in Initial frame
r_OBm = T_bar_B*[ 0 0 h_B*0.5 1].';
x_OBm = r_OBm(1,1);

% Task space coordinate d in order to keep the feet underneath the base
d = x_OBm - x_OFm;


% Definition of the task space coordinates
% x_Ts = [q_j(3)-pi/2, q_j(1)-T_bar_l(3,4), q_j(1)-T_bar_r(3,4) ].';

%additional task space coordinate
x_Ts = [q_j(3)-pi/2, q_j(1)-T_bar_l(3,4), z_Hr -T_bar_r(3,4) , d].';


% Number of base coordinates
nb=3;

%Number of simplification steps
ns = 100;

% Generate numerical functions
robotmdl.generatetaskspacefunctions(x_Ts,nb,ns);
%% Save to MAT File

% Generate file and directory paths
fname = mfilename;
fpath = mfilename('fullpath');
dpath = strrep(fpath, fname, '');

% Store generated model in the appropriate directory
save(strcat(dpath,robotmdl.name), 'robotmdl');


%% 
% EOF
