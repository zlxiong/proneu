% proNEu: A tool for rigid multi-body mechanics in robotics.
% 
% Copyright (C) 2017  Marco Hutter, Christian Gehring, C. Dario Bellicoso,
% Vassilios Tsounis
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%   File:           quadruped2d_simulation.m
%
%   Authors:        Vassilios Tsounis, tsounisv@ethz.ch
%
%   Date:           23/01/2018
%
%   Desciption:     A simple controller for an under-actuated double
%                   pendulum cart-pole system.
%

function [ tau ] = quadruped2d_controller(model,world,logger, t, x)
    
    % ARGUMETNS:
    %
    % model: the robot model object
    % world: the world model object
    % logger: data logger instance <ingore this argument>
    % t: The current time
    % x: The state of the robot, x := [q_B; u_B; q_j; dq_j]

    % Base configuration and velocities
    % --> These would be computed by a state-estimator from IMU data
    q_b = x(1:3);
    dq_b = x(8:10);
   
    % Joint configurations and velocities
    % --> These would be computed by a state-estimator from actuator data
    q_j = x(4:7);
    dq_j = x(11:14);

    % Initialize output torque
    Nj = 4;
    tau_star = zeros(Nj,1);
    
    %% DEMO JOINT-FREEZE CONTROLLER
    % Desired nominal joint configurations
    q_j_star = [pi/4 -pi/2 -pi/4 pi/2].';
    dq_j_star = zeros(Nj,1);
    tau_frz = zeros(Nj,1);
    
    i = 1;
    tau_frz(i) = 1500.0*(q_j_star(i) - q_j(i)) + 150.0*(dq_j_star(i) - dq_j(i));
    i = 2;
    tau_frz(i) = 500.0*(q_j_star(i) - q_j(i)) + 50.0*(dq_j_star(i) - dq_j(i));
    i = 3;
    tau_frz(i) = 1500.0*(q_j_star(i) - q_j(i)) + 150.0*(dq_j_star(i) - dq_j(i));
    i = 4;
    tau_frz(i) = 500.0*(q_j_star(i) - q_j(i)) + 50.0*(dq_j_star(i) - dq_j(i));
    
    % PRINTOUT
%     proneu_info('tau:');
%     tau
    
    %% Gravity Compensation
    % Set simulation data
    mparams = model.parameters.values;
    
    % Get the system generalized coordinates and velocities
    q_data = x(1:7);
    dq_data = x(8:14);
    
    % Extract kinematic information of the front foot.
    T_Fc = model.body(4).kinematics.compute.T_IBi(q_data,[],[],mparams);
    J_Fc = model.body(4).kinematics.compute.I_J_IBi(q_data,[],[],mparams);
    
    % Extract kinematic information of the rear foot.
    T_Rc = model.body(7).kinematics.compute.T_IBi(q_data,[],[],mparams);
    J_Rc = model.body(7).kinematics.compute.I_J_IBi(q_data,[],[],mparams);
    
    % Feet positions
    r_Fc = T_Fc(1:3,4);
    r_Rc = T_Rc(1:3,4);
    
    % Dynamic quantities
    S = model.dynamics.compute.S_a(q_data,[],[],mparams);
    g = model.dynamics.compute.g(q_data,[],[],[],mparams);
    
    % Contact forces
    f_Fc = [0 0 156.9600 0 0 0].';
    f_Rc = [0 0 156.9600 0 0 0].';
    
    % Compute standard GC term for leg gravity
    tau_gc = S*g;
    
    % Add additional term if front leg is in contact
    if (r_Fc(3) <= 0)
        tau_gc = tau_gc + S*(J_Fc.'*f_Fc);
    end
    
    % Add additional term if rear leg is in contact
    if (r_Rc(3) <= 0)
        tau_gc = tau_gc + S*(J_Rc.'*f_Rc);
    end
    
    % PRINTOUT
    proneu_info('tau_gc:');
    tau_gc
    
    %% Final output
    
    tau_star = tau_frz;
    tau = tau_star;
    
end