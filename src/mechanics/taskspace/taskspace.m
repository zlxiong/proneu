% proNEu: A tool for rigid multi-body mechanics in robotics.
% 
% Copyright (C) 2017  Marco Hutter, Christian Gehring, C. Dario Bellicoso,
% Vassilios Tsounis
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

%%
%   File:           taskspace.m
%
%   Authors:        Manuel Breitenstein, bmanuel@ethz.ch
%
%   Date:           02/11/2017
%
%   Desciption:     Creates function handles for task space jacobians. The
%                   functions are created corresponding to [1].
%
function [] = taskspace(model,x_Ts,nb,ns)
    proneu_info('Creating Taskspace function handles.');
    % Number of bodies
    n = size(model.body,2);
    
    % Number of Task space coordinates
    nTs = size(x_Ts,1);
    
    % Get generalized coordinate
    q = model.dynamics.symbols.q(1:n);
    qr = q(nb+1:end);
    dq = model.dynamics.symbols.dq(1:n);
    dqr = dq(nb+1:end);
    params = model.parameters.symbols;

    % Derivative of taskspace coordinates
    dx_Ts= dAdt(x_Ts,q,dq);
    
    %   ddx_Ts= dAdt(dx_Ts,dq,ddq)
    %   h_ddx_Ts = matlabFunction(ddx_Ts, 'Vars', {params, dq, ddq});
    
    %Dynamics including the base
    % M_sys*[q_b q_r].' + b_sys + g_sys=[0 T].'
    
    % Mass/inertia matrix including the base
    M_sys = model.dynamics.symbols.M;
    proneu_info('-> Computing the inverse of the inertia matrix')
    inv_M_sys = simplify(eye(size(M_sys,1))/M_sys,ns);
    % Block component of the Mass/inertia matrix corresponding to the base
    M_sys_bb = M_sys(1:nb,1:nb);
    proneu_info('-> Computing the inverse of the base inertia matrix')
    inv_M_sys_bb = simplify(eye(nb)/M_sys_bb,ns);
    % Block component of the Mass/inertia matrix corresponding to the base
    M_sys_br = M_sys(1:nb,nb+1:end);
    
    b_sys = model.dynamics.symbols.b;
    g_sys = model.dynamics.symbols.g;
    
    % Taskspace Jacobian including the base components.
    % dx_Ts = I_J_sys * [qb qr].'
    proneu_info('-> Simplifying Task space jacobian');
    I_J_sys = simplify(jacobian(x_Ts,q), ns);
    % Block of the jacobian corresponding to the base
    I_J_sys_b = I_J_sys(:,1:nb);
    % Block of the jacobian corresponding to the joints
    I_J_sys_r = I_J_sys(:,nb+1:end);
    
    %Dynamics in joint spae
    % M*q_r + b + g = T
    
    % Generalized Jacobian of the task space.
    % dx_Ts = I_J* dqr
    proneu_info('-> Simplifying Generalized Jacobian');
    % S_q_bar = [-inv_M_sys_bb*M_sys_br; eye(n-nb)];
    I_J = simplify(I_J_sys_r - I_J_sys_b*inv_M_sys_bb*M_sys_br,ns);
    % I_J = simplify(I_J_sys*S_q_bar, ns);
    proneu_info('-> Simplifying time derivative of the Generalized Jacobian');
    dI_J = simplify(dAdt(I_J,q,dq),ns);
   
    % Selection Matrix for joint generalized coordinates
    S_q = [zeros(n-nb,nb) eye(n-nb)];
    
    proneu_info('-> Computing joint space inertia matrix');
    M = simplify(eye((n-nb)) / (S_q*inv_M_sys*S_q'),ns);
    proneu_info('-> Computing inverse of joint space inertia matrix');
    inv_M = simplify(eye(size(M,1))/M,ns);
    
    % Dynamically consistent generelized inverse of selection matrix
    S_q_bar = simplify(inv_M_sys*S_q.'*M,ns);
    
    % Coriolis forces of free floating system
    b_r = simplify(S_q_bar'*b_sys,ns);
    g_r = simplify(S_q_bar'*g_sys,ns);
        
    % M = inv((S_q*inv_M_sys*S_q'));

    % Calculate task space EoM
    % lambda*ddq_Ts + mu + p = F
   
    proneu_info('-> Simplifying task space inertia matrix');
    lambda = eye(nTs)/(I_J*inv_M*I_J');
    
    proneu_info('-> Simplifying task space corriolis terms');
    mu = lambda*I_J*inv_M*b_r - lambda*dI_J*dqr;
    
    proneu_info('-> Simplifying task space gravity terms');
    p = lambda * I_J * inv_M * g_r; 

    
    %Create symbols and function handles
    proneu_info('--> Generating numeric functions for the task space coordinates');
    model.controller.symbols.x_Ts = x_Ts;
    model.controller.compute.x_Ts  = matlabFunction(x_Ts, 'Vars',{params,q,dq});
    
    model.controller.symbols.dx_Ts = dx_Ts;
    model.controller.compute.dx_Ts = matlabFunction(dx_Ts, 'Vars', {params, q, dq});
    
    proneu_info('--> Generating numeric functions for the task space Jacobian');
    model.controller.symbols.I_J_sys = I_J_sys;
    model.controller.compute.I_J_sys = matlabFunction(I_J_sys,'Vars',{params,q,dq});
    
    proneu_info('--> Generating numeric functions for the Generalized Jacobian');
    model.controller.symbols.I_J_g = I_J;
    model.controller.compute.I_J_g = matlabFunction(I_J,'Vars',{params,q,dq});
    
    proneu_info('--> Generating numeric functions for the time derivative of the Generalized Jacobian');
    model.controller.symbols.dI_J_g = dI_J;
  %  model.controller.compute.dI_J_g = matlabFunction(dI_J,'Vars',{params,q,dq});
        
    proneu_info('--> Generating numeric functions for the task space inertia matrix');
    model.controller.symbols.lambda = lambda;
    model.controller.compute.lambda = matlabFunction(lambda,'Vars',{params,q,dq});
    
    proneu_info('--> Generating numeric functions for the task space corriolis terms');
    model.controller.symbols.mu = mu;
    model.controller.compute.mu = matlabFunction(mu,'Vars',{params,q,dq});
    
    proneu_info('--> Generating numeric functions for the task space gravity terms');
    model.controller.symbols.p = p;
    model.controller.compute.p = matlabFunction(p,'Vars',{params,q,dq}); 
    
    %Creating only symbolic values for the folowing
    model.controller.symbols.M = M;
    model.controller.symbols.inv_M = inv_M;
    model.controller.symbols.b = b_r;
    model.controller.symbols.g = g_r;
    model.controller.symbols.S_q_bar = S_q_bar;
    model.controller.symbols.S_q = S_q;
end

        
%% References
%
% [1]    L. Sentis and O. Khatib, “Control of free-floating humanoid robots
%        through task prioritization,” Proc. - IEEE Int. Conf. Robot.
%        Autom., vol. 2005, no. April, pp. 1718–1723, 2005.
